;;; rc-irony-cedet.el --- Irony/CEDET integration.   -*- lexical-binding: t; -*-

(require 'irony-cdb)
(require 'cl-lib)
(require 'cedet-cpp-project)

(defun irony-cdb-cedet (command &rest args)
  (cl-case command
    (get-compile-options (irony-cdb-cedet--get-compile-options))))

(defun irony-cdb-cedet--get-compile-options ()
  (irony--awhen (cedet-cpp-project/load-project)
    (irony-cdb-cedet--load-flags it)))

(defun irony-cdb-cedet--load-flags (project)
  (list
   (cons
    ;; compile options with trailing whitespaces removed
    (cedet-cpp-project/make-clang-flags project)
    ;; pch either doesn't work with irony or requires a different command line.
    ;; (append (cedet-cpp-project/make-clang-flags project)
    ;;         `("-Xclang" "-include-pch" "-Xclang" ,(cedet-cpp-project/prefix-header project)))
    ;; working directory
    (ede-project-root-directory project))))

(provide 'rc-irony-cedet)
;;; rc-irony-cedet.el ends here
