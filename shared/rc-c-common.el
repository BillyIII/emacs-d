;;; rc-c-common.el --- C/C++ modes configuration.    -*- lexical-binding: t; -*-

;; Default definition with camel instead of snake.
;; (defun my/electric-case-c-init ()

;;   (electric-case-mode 1)
;;   (setq electric-case-max-iteration 2)

;;   (setq electric-case-criteria
;;         (lambda (b e)
;;           (let ((proper (electric-case--possible-properties b e))
;;                 (key (key-description (this-single-command-keys))))
;;             (cond
;;              ((member 'font-lock-variable-name-face proper)
;;               ;; #ifdef A_MACRO  /  int variable_name;
;;               (if (member '(cpp-macro) (c-guess-basic-syntax)) 'usnake 'camel))
;;              ((member 'font-lock-string-face proper) nil)
;;              ((member 'font-lock-comment-face proper) nil)
;;              ((member 'font-lock-keyword-face proper) nil)
;;              ((member 'font-lock-function-name-face proper) 'camel)
;;              ((member 'font-lock-type-face proper) 'ucamel)
;;              (electric-case-convert-calls 'camel)
;;              (t nil)))))

;;   (defadvice electric-case-trigger (around electric-case-c-try-semi activate)
;;     (when (and electric-case-mode
;;                (eq major-mode 'c-mode))
;;       (if (not (string= (key-description (this-single-command-keys)) ";"))
;;           ad-do-it
;;         (insert ";")
;;         (backward-char)
;;         ad-do-it
;;         (delete-char 1))))
;;   )

(require 'irony)
(require 'irony-cdb)


(add-to-list 'auto-mode-alist '("\\.ipp\\'" . c++-mode))

(defun my/cc-ident-hook ()
  (c-set-offset 'substatement-open 0)

  (setq c++-tab-always-indent t)
  ;; (setq c-basic-offset 4)                  ;; Default is 2
  ;; (setq c-indent-level 4)                  ;; Default is 2

  ;; (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60))
  (setq tab-width 4)
  (setq indent-tabs-modei nil)
  ;; (my/electric-case-c-init)
  (c-set-style "gnu")
  )

(defun my/cc-extras-hook ()
  ;; (highlight-80+-mode)
  (rainbow-delimiters-mode)
  (highlight-parentheses-mode)
  ;; (highline-mode)
  ;; (highlight-tabs)
  ;; (highlight-trailing-whitespace)
  )

(add-hook 'c-mode-common-hook 'my/cc-ident-hook)
(add-hook 'c-mode-common-hook 'my/cc-extras-hook)

;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/shared/emacs-clang-complete-async"))
;; (require 'auto-complete)
;; (require 'auto-complete-clang-async)

;; (define-key ac-mode-map  [(meta tab)] 'auto-complete)

;; (defun my/ac-cc-mode-setup ()
;;   (setq ac-sources (append '(ac-source-clang) ac-sources)))

;; (add-hook 'c-mode-common-hook 'my/ac-cc-mode-setup)

;; (require 'auto-complete-config)
;;(require 'auto-complete-clang)

;; (require 'cedet-clang-autocomplete)

;; (setq-default cedet-cpp-project/default-clang-flags '("-xc++" "-std=c++1b"))

(setq ac-auto-start nil)
(setq ac-quick-help-delay 0.5)
(setq ac-show-menu-immediately-on-auto-complete t)
(setq ac-auto-show-menu t)
(setq ac-expand-on-auto-complete t)
;;(ac-set-trigger-key "TAB")
;;(define-key ac-mode-map (kbd "M-TAB") 'cedet-clang-ac/auto-complete)
;;(define-key ac-completing-map (kbd "M-t") 'ac-next)
;;(define-key ac-completing-map (kbd "M-c") 'ac-previous)

;; (defun my/ac-config ()
;;   (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))
;;   (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
;;   (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
;;   (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)
;;   (add-hook 'css-mode-hook 'ac-css-mode-setup)
;;   (add-hook 'auto-complete-mode-hook 'ac-common-setup)
;;   (global-auto-complete-mode t))

;; (defun my/ac-cc-mode-setup ()
;;   (setq ac-clang-complete-executable "clang-complete")
;;   ;; (setq ac-clang-complete-executable (expand-file-name "~/.emacs.d/shared/emacs-clang-complete-async/clang-complete"))
;;   (setq-default ac-clang-cflags '("-xc++" "-std=c++1z"))
;;   ;; (setq-default ac-clang-cflags '("-I/usr/include/clang-c" "-I/usr/include" "-I/usr/include/boost/tr1/tr1" "-I/usr/include/boost/compatibility/cpp_c_headers" "-I/usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.2" "-std=c++1y" "-xc++"))
;;   ;; (setq ac-sources '(ac-source-clang-async))
;;   ;; (ac-clang-launch-completion-process)
;;   ;; (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources))
;;   (add-to-list 'ac-omni-completion-sources
;;                (cons "\\." '(ac-source-clang-async)))
;;   (add-to-list 'ac-omni-completion-sources
;;                (cons "->" '(ac-source-clang-async)))
;;   (irony-mode))

;; (add-hook 'c-mode-common-hook 'my/ac-cc-mode-setup)
;; ac-source-gtags
;; (my/ac-config)

;; (eval-after-load 'company
;;   '(add-to-list 'company-backends 'company-irony))
;; (eval-after-load 'company
  ;; '(setq company-backends (cons 'company-irony (remove 'company-semantic company-backends))))

;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
;; (add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)

;; (defun my/irony-mode-hook ()
;;   (define-key irony-mode-map [remap completion-at-point]
;;     'irony-completion-at-point-async)
;;   (define-key irony-mode-map [remap complete-symbol]
;;     'irony-completion-at-point-async))

;; (add-hook 'irony-mode-hook 'my/irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(add-hook 'c-mode-common-hook 'irony-mode)
(add-hook 'c-mode-common-hook 'company-mode)


(defun my/c-indent-or-complete ()
  (interactive)
  (if (or (looking-at "\\>")
          (looking-back "\\>\\.")
          (looking-back "\\>->")
          (looking-back "\\>::")
          )
      (company-complete)
    (indent-for-tab-command)
    ))

(defun my/c-company-hook ()
  (local-set-key (kbd "<tab>") 'my/c-indent-or-complete)
  )

(add-hook 'c-mode-common-hook 'my/c-company-hook)


;; (require 'rc-irony-cedet)

;; (add-to-list 'irony-cdb-compilation-databases 'irony-cdb-cedet)

(provide 'rc-c-common)
;;; rc-c-common.el ends here
