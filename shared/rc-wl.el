;;; rc-wl.el --- WanderLust configuration.   -*- lexical-binding: t; -*-

(require 'wl)
(require 'elmo)

; Days since last access, i.e atime. (default: 50)
(elmo-cache-expire-by-age  14)
 
; Kilobytes. (default 30000)
(elmo-cache-expire-by-size 10000)

(setq wl-local-domain "localhost")

;; (setq wl-biff-check-folder-list '("%INBOX:me_billyiii.me/clear@mail.mxes.net:993!"
;;                                   "%_INBOX_Sorted/Games:me_billyiii.me/clear@mail.mxes.net:993!"
;;                                   "%_INBOX_Sorted/Social:me_billyiii.me/clear@mail.mxes.net:993!"))

(setq wl-biff-check-interval 900)

;; (setq wl-summary-force-prefetch-folder-list '("^%_Sync:"))

;; (setq wl-dispose-folder-alist '((":me_billyiii\\.me/" . "%Trash:me_billyiii.me/clear@mail.mxes.net:993!")))

(defvar my/wl-from-width 35)

(setq wl-summary-line-format (concat "%n%T%P%M/%D(%W)%h:%m %t%[%" (number-to-string my/wl-from-width) "(%c %f%) %] %s"))
(setq wl-summary-width 135)

;; http://xahlee.info/comp/in-place_algorithm.html
(defun my/reverse-string (str)
  (let ((len (length str)))
    (dotimes (i (floor (/ len 2)))
      (let (x)
        (setq x (aref str i))
        (aset str i (aref str (- (1- len) i)))
        (aset str (- (1- len) i) x) ) )))

(defsubst my/extract-address-domain (str)
  (cond ((and (stringp str) (string-match "[^ \t\n]*@\\([^ \t\n]*\\)" str))
         (wl-match-string 1 str))
        (t str)))

(defun my/components-to-summary-from (name address)
  (let* ((left-part (concat name " @"))
         (domain-width (max 0 (- my/wl-from-width (string-width left-part) 1)))
         (domain (or (my/extract-address-domain address) "<NONE>")))
    (my/reverse-string domain)
    (let ((revdomain (truncate-string-to-width domain domain-width nil nil "…")))
      (my/reverse-string revdomain)
      (concat left-part revdomain))))

(defun my/wl-summary-from (from)
  (let ((translator (if wl-use-petname
                        (lambda (string)
                          (or (funcall wl-summary-get-petname-function string)
                              (let ((components (std11-extract-address-components string)))
                                (and (car components) (my/components-to-summary-from (car components) (car (cdr components)))))
                              string))
                      #'identity))
        to ng)
    (or (and (eq major-mode 'wl-summary-mode)
             (stringp wl-summary-showto-folder-regexp)
             (string-match wl-summary-showto-folder-regexp
                           (wl-summary-buffer-folder-name))
             (wl-address-user-mail-address-p from)
             (cond
              ((setq to (elmo-message-entity-field wl-message-entity 'to))
               (concat "To:" (mapconcat translator to ",")))
              ((setq ng (elmo-message-entity-field wl-message-entity
                                                   'newsgroups))
               (concat "Ng:" ng))))
        (funcall translator from))))

(setq wl-summary-from-function 'my/wl-summary-from)

(setq wl-draft-send-mail-function 'wl-draft-send-mail-with-sendmail)

(provide 'rc-wl)

