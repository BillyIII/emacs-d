;;; cedet-clang-autocomplete.el --- Cedet project support for auto-complete-clang-async.  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Тима

;; Author: Тима <tima@tima-laptop>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; WARNING: Complex include paths must be quoted.

;; 

;;; Code:

(require 'auto-complete)
(require 'auto-complete-clang-async)
(require 'cedet-cpp-project)

(defvar cedet-clang-ac/current-project nil
  "Last project used to set clang settings.")

(defvar cedet-clang-ac/autoload-project t
  "(Bool) Load project on auto-complete.
Will be set to nil (locally) if project cannot be loaded.")
(make-variable-buffer-local 'cedet-clang-ac/autoload-project)

(defun cedet-clang-ac/force-update-clang-flags (&optional project)
  "Updates current clang cflags based on the PROJECT."
  (interactive)
  (let ((project (or project (cedet-cpp-project/load-project))))
    (setq ac-clang-cflags (cedet-cpp-project/make-clang-flags project))
    (setq ac-clang-prefix-header (cedet-cpp-project/prefix-header project))
    (setq cedet-clang-ac/current-project project)
    (ac-clang-update-cmdlineargs)
    t))

(defun cedet-clang-ac/reset-clang-flags ()
  "Resets current clang cflags to the default value."
  (interactive)
  (setq ac-clang-cflags (default-value 'ac-clang-cflags))
  (setq ac-clang-prefix-header nil)
  (setq cedet-clang-ac/current-project nil)
  (ac-clang-update-cmdlineargs)
  t)

(defun cedet-clang-ac/update-clang-flags ()
  "Updates current clang cflags based on the current project."
  (interactive)
  (let ((project (cedet-cpp-project/load-project)))
    (if project
        (if (not (eq project cedet-clang-ac/current-project))
            (cedet-clang-ac/force-update-clang-flags project)
          t)
      (cedet-clang-ac/reset-clang-flags)
      nil)))

(defun cedet-clang-ac/auto-complete ()
  "Loads project if necessary and invokes `auto-complete'."
  (interactive)
  (unwind-protect
      (progn
        (when (not ac-clang-completion-process)
          (ac-clang-launch-completion-process))
        (when cedet-clang-ac/autoload-project
          (when (not (cedet-clang-ac/update-clang-flags))
            (setq cedet-clang-ac/autoload-project nil))))
    (auto-complete)))

(defun cedet-clang-ac/restart-clang ()
  "Tries to kill and restart clang."
  (interactive)
  (kill-buffer "*clang-complete*") ;; <- This should trigger shutdown.
  (ac-clang-launch-completion-process)
  (cedet-clang-ac/force-update-clang-flags))

(provide 'cedet-clang-autocomplete)
;;; cedet-clang-autocomplete.el ends here
