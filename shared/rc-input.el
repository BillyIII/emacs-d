;;; rc-input.el --- Generic input configuration.     -*- lexical-binding: t; -*-

;;
;; xah-fly-keys
;;

(add-to-list 'load-path "~/.emacs.d/shared/xah-fly-keys")
;; (setq xah-fly-use-meta-key t)
;; (setq xah-fly-unset-useless-key t)
(require 'xah-fly-keys)

;; single "m" is "correct" here
(xah-fly-keys-set-layout "programer-dvorak")
(xah-fly-keys 1)

(global-set-key (kbd "C--") nil)
(global-set-key (kbd "C-=") nil)

(xah-fly--define-keys
 xah-fly-command-map
 '(("'" . xah-comment-dwim)
   (";" . xah-reformat-lines)
   ("l" . xah-fly-insert-mode-activate-space-before)
   ("L" . xah-insert-space-before)
   ("d" . my/xah-beginning-of-line-or-block)
   ("\d \d" . kill-current-buffer)
   ("B" . isearch-backward)
   ))

(xah-fly--define-keys
 xah-fly-leader-key-map
 '(("c >" . projectile-find-file-dwim)
   ("U" . projectile-switch-to-buffer)
   ("n v" . point-to-register)
   ))

(require 'persp-mode)
(require 'projectile)

(define-key xah-fly-leader-key-map (kbd (xah-fly--convert-kbd-str "q")) projectile-command-map)
(define-key xah-fly-leader-key-map (kbd (xah-fly--convert-kbd-str "v")) persp-key-map)

;; Does not skip the b-o-l jump on double press.
(defun my/xah-beginning-of-line-or-block ()
  "Move cursor to beginning of line or previous block.

• When called first time, move cursor to beginning of char in current line. (if already, move to beginning of line.)
• When called again, move cursor backward by jumping over any sequence of whitespaces containing 2 blank lines.
• if `visual-line-mode' is on, beginning of line means visual line.

URL `http://xahlee.info/emacs/emacs/emacs_keybinding_design_beginning-of-line-or-block.html'
Version: 2018-06-04 2021-03-16 2022-03-30 2022-07-03 2022-07-06"
  (interactive)
  (let ((xp (point)))
    (if (or (equal (point) (line-beginning-position))
            ;; (eq last-command this-command) ; REMOVED
            )
        (when
            (re-search-backward "\n[\t\n ]*\n+" nil 1)
          (skip-chars-backward "\n\t ")
          (forward-char))
      (if visual-line-mode
          (beginning-of-visual-line)
        (if (eq major-mode 'eshell-mode)
            (progn
              (declare-function eshell-bol "esh-mode.el" ())
              (eshell-bol))
          (back-to-indentation)
          (when (eq xp (point))
            (beginning-of-line)))))))

(defun my/chorded-key (ch k)
  (kbd (concat ch (xah-fly--convert-kbd-str k))))

;;
;; Russian layout fix.
;;

;; (defvar my/jcuken-to-dvorak-map '())
;; (defvar my/dvorak-to-jcuken-map '())
;; (defvar my/current-to-jcuken-map '())

;; ;; Letters only. Numbers and punctuation exist in both layouts and
;; ;; can't be translated without additional context (wrapper functions?).
;; (cl-loop
;;  for from across "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ"
;;  for to   across "',.pyfgcrl/=aoeuidhtns-;qjkxbmwvz\"<>PYFGCRL?+AOEUIDHTNS_:QJKXBMWV"
;;  do
;;  (push (cons (string from) (string to)) my/jcuken-to-dvorak-map)
;;  (push (cons (string to) (string from)) my/dvorak-to-jcuken-map))

;; (let ((d2j-to-c2j #'(lambda (kv)
;;                       (if-let ((kv2 (assoc (car kv) xah-fly--key-convert-table)))
;;                           (cons (cdr kv2) (cdr kv))
;;                         kv))))
;;   (setq my/current-to-jcuken-map (mapcar d2j-to-c2j my/dvorak-to-jcuken-map)))

;; (cl-loop
;;  for kv in my/jcuken-to-dvorak-map
;;  do
;;  (let ((tr (xah-fly--convert-kbd-str (cdr kv))))
;;    (define-key key-translation-map (kbd (concat "C-" (car kv))) (kbd (concat "C-" tr)))
;;    (define-key key-translation-map (kbd (concat "M-" (car kv))) (kbd (concat "M-" tr)))
;;    (define-key key-translation-map (kbd (concat "M-C-" (car kv))) (kbd (concat "M-C-" tr)))))

;; (defun my/localize-keymap (keys)
;;   (map-keymap #'(lambda (e f)
;;                   (unless (symbolp e)
;;                     (let* ((oldstr (key-description (make-vector 1 e)))
;;                            (newstr (let ((xah-fly--key-convert-table my/current-to-jcuken-map))
;;                                      (xah-fly--convert-kbd-str oldstr)))
;;                            (newkbd (kbd newstr)))
;;                       (unless (lookup-key keys newkbd)
;;                         (define-key keys newkbd f))))
;;                   (if (keymapp f)
;;                       (my/localize-keymap f)))
;;               keys))

;; (add-hook 'after-init-hook #'(lambda () (my/localize-keymap xah-fly-command-map)))

;; ;; Ignore non-ascii key representations in the which-key output.
;; ;; Replace with cyrillic specifically (0400-04ff) if it starts causing issues.
;; (require 'which-key)
;; (push '(("[\u007f-\U0010ffff]") . "nope") which-key-replacement-alist)

(require 'quail-pdvorak-jcuken)
(setq default-input-method "cyrillic-pdvorak")

;;
;; Company.
;;

(require 'company)
(require 'company-irony)

(add-hook 'after-init-hook 'global-company-mode)

(define-key company-mode-map [remap completion-at-point] 'company-complete)
(define-key company-mode-map [remap complete-symbol] 'company-complete)
(define-key company-active-map (my/chorded-key "M-" "c") 'company-select-previous)
(define-key company-active-map (my/chorded-key "M-" "t") 'company-select-next)
(define-key company-active-map (my/chorded-key "M-" "C") 'company-previous-page)
(define-key company-active-map (my/chorded-key "M-" "T") 'company-next-page)
(define-key company-active-map (my/chorded-key "M-" "m") 'company-complete-selection)

;;
;; Bookmarks.
;;

(defun my/set-quick-jump-keys (set-key jump-key register)
  (let ((bound-register register))
    (global-set-key set-key (lambda () (interactive)
                              (point-to-register bound-register)))
    (global-set-key jump-key (lambda () (interactive)
                               (jump-to-register bound-register)))
    (get-register register)
    )
  )

(defun my/set-numeric-jump-keys ()
  (interactive)
  (my/set-quick-jump-keys (my/chorded-key "M-" "!") (my/chorded-key "M-" "1") 1)
  (my/set-quick-jump-keys (my/chorded-key "M-" "@") (my/chorded-key "M-" "2") 2)
  (my/set-quick-jump-keys (my/chorded-key "M-" "#") (my/chorded-key "M-" "3") 3)
  (my/set-quick-jump-keys (my/chorded-key "M-" "$") (my/chorded-key "M-" "4") 4)
  (my/set-quick-jump-keys (my/chorded-key "M-" "%") (my/chorded-key "M-" "5") 5)
  (my/set-quick-jump-keys (my/chorded-key "M-" "^") (my/chorded-key "M-" "6") 6)
  (my/set-quick-jump-keys (my/chorded-key "M-" "&") (my/chorded-key "M-" "7") 7)
  (my/set-quick-jump-keys (my/chorded-key "M-" "*") (my/chorded-key "M-" "8") 8)
  (my/set-quick-jump-keys (my/chorded-key "M-" "(") (my/chorded-key "M-" "9") 9)
  (my/set-quick-jump-keys (my/chorded-key "M-" ")") (my/chorded-key "M-" "0") 0)
  )

(add-hook 'after-init-hook 'my/set-numeric-jump-keys)

;;
;; Misc. stuff.
;;

(global-set-key (kbd "<C-tab>") nil)
(global-set-key (kbd "<C-S-tab>") nil)
(global-set-key (my/chorded-key "M-" "c") 'scroll-down)
(global-set-key (my/chorded-key "M-" "t") 'scroll-up)

(global-subword-mode t)

(provide 'rc-input)
;;; rc-input.el ends here
