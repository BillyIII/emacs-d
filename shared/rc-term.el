;;; rc-term.el --- Terminal-related configuration.   -*- lexical-binding: t; -*-


;; (defvar eshell-history-ring (make-ring eshell-history-size))
;; (setq eshell-history-ring (make-ring 4096))
;; (when (boundp eshell-history-ring) (makunbound eshell-history-ring))
;; (defvar eshell-history-ring nil)

(require 'eat)
(add-hook 'eshell-load-hook #'eat-eshell-mode)

(require 'eshell)

;; (add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode)
;; (remove-hook 'eshell-load-hook #'eat-eshell-visual-command-mode)

(defun my/around-eshell-hist-initialize (fun &rest args)
  (let ((res (apply fun args))
        (new-hist eshell-history-ring))
    (kill-local-variable 'eshell-history-ring)
    (unless eshell-history-ring
      (setq eshell-history-ring new-hist))
    res))

(advice-add 'eshell-hist-initialize :around 'my/around-eshell-hist-initialize)

;; (setq eat-semi-char-non-bound-keys (cons [M-space]
;; eat-semi-char-non-bound-keys))

(defun my/around-eat-term-make-keymap (fun &rest args)
  (let* ((map (apply fun args))
         (meta (lookup-key map [27])))
    (when (and meta (keymapp meta))
      (define-key meta [32] nil t))
    ;; (define-key map [M-<space>] nil t)
    map))

(advice-add 'eat-term-make-keymap :around 'my/around-eat-term-make-keymap)

(eat-update-semi-char-mode-map)
(eat-reload)

(require 'eshell)
(setq eshell-prefer-lisp-functions t)

(require 'eshell-bookmark)
(add-hook 'eshell-mode-hook #'eshell-bookmark-setup)

(require 'eshell-autojump)

(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'N))

(defun my/dtach-attach ()
  (interactive)
  (let* ((sessions (string-lines (shell-command-to-string "dtach-list")))
         (session (completing-read "Choose a session: " sessions))
         (buffer (eat (concat "dtach-attach " session " -r winch"))))
    (with-current-buffer buffer
      (rename-buffer (concat "*dtach " session "*") t)
      (add-hook
       'eat-exit-hook
       (lambda (process)
         (let ((status (process-exit-status process)))
           (message "dtach session %S has exited with code %S" session status)
           (when (eq 0 status)
             (kill-buffer buffer))))
       90 t))))

(xah-fly--define-keys
 xah-fly-leader-key-map
 '(("n E" . eshell-new)
   ("n D" . my/dtach-attach)))


(provide 'rc-term)
;;; rc-term.el ends here
