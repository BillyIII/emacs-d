;;; rc-www.el --- 	WWW-related configuration.                       -*- lexical-binding: t; -*-

(require 'eww)
(require 'shr)
(require 'eww-lnum)


(setq eww-download-directory "~/downloads/")
(setq shr-color-visible-luminance-min 60)
;; (setq shr-color-visible-distance-min 5)
(setq shr-use-fonts t)
(setq browse-url-browser-function 'eww-browse-url)
;; (setq browse-url-generic-program "palemoon")

(require 'persp-mode)

(defun my/setup-eww-buffer ()
  (interactive)
  (when (eq major-mode 'eww-mode )
    (if (persp-buffer-free-p)
        (persp-add-buffer (current-buffer)
                          (or (get-current-persp) (persp-add-new "eww"))))
    (let* ((title (plist-get eww-data :title))
           (url (plist-get eww-data :url))
           (btitle (if (> (length title) 0) title url)))
      (rename-buffer (concat "eww " btitle ) t))))

(add-hook 'eww-after-render-hook 'my/setup-eww-buffer)

;; (defun my/eww-mode-hook ()
;;   (rename-buffer "eww" t))

;; (add-hook 'eww-mode-hook 'my/eww-mode-hook)))


(defun my/persp-save-eww-buffer (buffer tag vars)
 (list tag (buffer-name buffer) (plist-get (alist-get 'eww-data vars) :url)))

(defun my/persp-load-eww-buffer (savelist default-load-fun after-load-fun)
  (cl-destructuring-bind (tag bname url) savelist
    (let ((buffer (generate-new-buffer bname)))
      (with-current-buffer buffer
        (eww-mode)
        (eww url))
      buffer)))

(persp-def-buffer-save/load
 :mode 'eww-mode
 :tag-symbol 'eww-url
 :save-vars (list 'eww-data)
 :save-function 'my/persp-save-eww-buffer
 :load-function 'my/persp-load-eww-buffer)


(defun my/eww-readable ()
  (interactive)
  (ignore-errors (eww-readable))
  (my/setup-reading-buffer))


(define-key eww-buffers-mode-map (kbd "d") 'eww-buffer-kill)

(define-key eww-mode-map "f" 'eww-lnum-follow)
(define-key eww-mode-map "F" 'eww-lnum-universal)
;; (define-key eww-mode-map "q" 'kill-current-buffer)
(define-key eww-mode-map "R" 'my/eww-readable)

;; (setq w3m-minor-mode-map (make-sparse-keymap))
;; (setq w3m-mode-map (make-sparse-keymap))

;; (defun my/mime-w3m-setup-hook ()
;;   (setq w3m-minor-mode-map (make-sparse-keymap))
;;   (setq w3m-mode-map (make-sparse-keymap)))

;; (add-hook 'mime-w3m-setup-hook 'my/mime-w3m-setup-hook)

;; Remove all shr bindings, they shadow mine when applied to regions.
;; (setq shr-map (make-sparse-keymap))
;; (setq eww-link-keymap
;;   (let ((map (copy-keymap shr-map)))
;;     (define-key map "\r" 'eww-follow-link)
;;     map))


(provide 'rc-www)
;;; rc-www.el ends here
