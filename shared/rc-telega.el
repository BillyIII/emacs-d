;;; rc-telega.el --- telega.el config                             -*- lexical-binding: t; -*-

(setq telega-use-images t)
;; (setq telega-server-logfile nil)
(setq telega-server-verbosity 1)
;; (setq telega-server-libs-prefix (expand-file-name "~/src/td/build"))
(setq telega-server-libs-prefix (expand-file-name "~/.local"))
;; (setq telega-server-command "telega-server-fixed")
(setq telega-chat-send-message-on-ret 'if-at-the-end)
(setq telega-emoji-font-family "EmojiOne Color")
(setq telega-emoji-use-images nil)
(setq telega-emoji-use-images nil)
(setq telega-chat-input-markups '("org" nil "markdown2"))
(setq telega-sticker-animated-play t)
(setq telega-mode-line-string-format '(" 🗼"
    (:eval (telega-mode-line-online-status))
    ;; (:eval (when telega-use-tracking-for
             ;; (telega-mode-line-tracking)))
    (:eval (telega-mode-line-unread-unmuted))
    (:eval (telega-mode-line-mentions 'messages))))
(setq telega-video-player-command
      '(concat "mpv"
               (when telega-ffplay-media-timestamp
                 (format " --start=%f" telega-ffplay-media-timestamp))))

(defun my/telega-emoji-init-fix ()
  ;; Otherwise custom emoji names are overridden by the built-in ones.
  (setq telega-emoji-alist (reverse telega-emoji-alist)))

(advice-add 'telega-emoji-init :after 'my/telega-emoji-init-fix)

(defun my/telega-reload-emoji ()
  (interactive)
  (setq telega-emoji-alist nil)
  (telega-emoji-init))
    
(require 'telega)

(setq telega-emoji-custom-alist '((":thumb-up:" . "👍")
                                  (":love-heart:" . "❤")
                                  (":stone-face:" . "🗿")
                                  (":person-love-hearts:" . "🥰")
                                  (":fire-love-heart:" . "❤‍🔥")
                                  (":tear-crying:" . "😢")
                                  (":tears-crying:" . "😭")
                                  (":angry-rage-swearing:" . "🤬")
                                  (":shrug-woman-actual:" . "🤷‍♀")))
(my/telega-reload-emoji)

(telega-mode-line-mode 1)

(provide 'rc-telega)
;;; rc-telega.el ends here
