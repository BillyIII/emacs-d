;;; rc-files.el --- Disk-browsing-related configuration.     -*- lexical-binding: t; -*-

;; (require 'ranger)
;; (setq ranger-hide-cursor nil)
;; Borks some hooks somewhere. Started recently-ish (2024-07-16).
;; Can't even debug it properly due to everything failing.
;; (ranger-override-dired-mode t)

;; Overrides the original.
;; Cursor is hidden unconditionally elsewhere in rangel.el, and not restored.
;; Also, highlights are obnoxious.
(defun ranger-hide-the-cursor ()
  (if (and buffer-read-only ranger-hide-cursor)
      (setq-local cursor-type nil)
    (setq-local cursor-type t)) ;; ADDED
  ;; (hl-line-mode t) ;; REMOVED
  )

(defun my/dired-jump-or-command ()
  (interactive)
  (if (derived-mode-p 'ranger-mode)
      (set-transient-map ranger-normal-mode-map)
    (dired-jump)))

(xah-fly--define-keys
 xah-fly-leader-key-map
 '(("m" . my/dired-jump-or-command)))

(setq my/dired-shell-command-image "sxiv -fo")
(setq my/dired-shell-command-video "mpv --audio-channels=stereo --force-window=yes &")

(setq dired-guess-shell-alist-user '(("\\.pdf\\'" "zathura&")
                                     
                                     ("\\.jpg\\'" my/dired-shell-command-image)
                                     ("\\.png\\'" my/dired-shell-command-image)
                                     ("\\.webp\\'" my/dired-shell-command-image)
                                     ("\\.gif\\'" my/dired-shell-command-image)
                                     ("\\.bmp\\'" my/dired-shell-command-image)
                                     
                                     ("\\.mp4\\'" my/dired-shell-command-video)
                                     ("\\.avi\\'" my/dired-shell-command-video)
                                     ("\\.mkv\\'" my/dired-shell-command-video)
                                     ("\\.ogv\\'" my/dired-shell-command-video)
                                     ))

(defun my/file-mimetype (path)
  (let ((filetype (with-temp-buffer
                    (call-process "file" nil t nil "--brief" "--mime-type" path)
                    (buffer-substring-no-properties (point-min) (point-max))))
        (parsed (split-string filetype "/" t "\n")))
    (if (and (listp parsed) (length= parsed 2))
        parsed
      nil)))

(setq my/open-file-by-mime-fns
      '(("video" nil my/open-file-video)
        ("audio" nil my/open-file-audio)
        ("image" nil my/open-file-image)
        ("application" "pdf" my/open-file-pdf)
        (nil nil my/open-file-default)))

;; (defun my/open-file (path &rest args)
;;   (let ((edit (plist-get args :edit))
;;         (gui (or (plist-get args :gui) (display-graphic-p)))
;;         (quiet (plist-get args :quiet)))
;;     (cl-destructuring-bind (major minor) (my/file-mimetype path)
;;       (let ((fn (cl-find-if (lambda (item)
;;                               (and
;;                                (or (car item) t
;;                             my/open-file-by-mime-fns)
;;       (((pred (string-equal "video")) minor)
;;        (my/open-file-video minor edit gui quiet))
;;       (((pred (string-equal "image")) minor)
;;        (my/open-file-image minor edit gui quiet))
;;       (((pred (string-equal "audio")) minor)
;;        (my/open-file-audio minor edit gui quiet))
;;       (((pred (string-equal "application")) (pred (string-equal "pdf")))
;;        (my/open-file-pdf edit gui quiet))
         
;;          ((pred (string-equal "audio")) (my/open-file-audio minor edit gui quiet))
;;          ((pred (string-equal "image"))
;;           (pcase subtype
;;             ((pred (string-equal "gif")) "g")
;;             ;; No idea which types should qualify as a "bitmap".
;;             ;; Any uncompressed image? Is this even neccessary?
;;             ;; https://raw.githubusercontent.com/jgoerzen/pygopherd/master/doc/standards/Gopher%2B.txt
;;             ;; ((pred (string-equal "bmp")) ":")
;;             (_ "I")))
;;          ((pred (string-equal "video")) ";")
;;          ((pred (string-equal "audio")) "<")
         
;;         "video" (my/open-file-video minor edit gui quiet)
;;         "audio" (my/open-file-audio minor edit gui quiet)
;;         "image" (my/open-file-image minor edit gui quiet)
;;         "application" (my/open-file-image minor edit gui quiet)

;;         (defun my/open-file (path &rest args)

(provide 'rc-files)
;;; rc-files.el ends here
