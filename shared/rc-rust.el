;;; rc-rust.el --- Rust mode configuration.    -*- lexical-binding: t; -*-

(require 'rust-mode)
;;(require 'racer)
(require 'company)

;; (with-eval-after-load 'racer
;;   (progn
;; (setq racer-cmd "/home/tima/.cargo/bin/racer")
;; (setq racer-cmd "/home/tima/src/racer/target/debug/racer")

;; (setq racer-rust-src-path "/home/tima/src/rust/library")
;; (setq racer-cargo-home "/home/tima/.cargo")

;; (add-hook 'rust-mode-hook #'racer-mode)

;; (add-hook 'racer-mode-hook #'eldoc-mode)
;; (add-hook 'racer-mode-hook #'company-mode)

;; (define-key racer-mode-map (kbd "M-.") nil)
;; (define-key racer-mode-map (kbd "M-,") nil)

;; (defun my/rust-mode-hook ()
  ;; (rustic-mode 1)
  ;; (yas-minor-mode-on)
  ;; )

;; (add-hook 'rust-mode-hook 'my/rust-mode-hook)

(defun my/rustic-mode-hook ()
  (yas-minor-mode-on)
  (setq electric-case-criteria (lambda (b e) 'snake))
  (electric-case-mode 1))

(add-hook 'rustic-mode-hook 'my/rustic-mode-hook)

(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)

(require 'lsp-rust)
(setq lsp-rust-analyzer-diagnostics-disabled ["unnecessary-braces" "unused_braces"])
(setq lsp-rust-analyzer-completion-auto-import-enable nil)

;; ))

;; (defun my/racer-mode-hook ()
;;   )

;; (add-hook 'racer-mode-hook 'my/racer-mode-hook)

(provide 'rc-rust)
;;; rc-rust.el ends here

