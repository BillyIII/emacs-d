;;; cedet-cpp-project.el --- Customized C++ project.  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Тима

;; Author: Тима <tima@tima-laptop>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'ede/cpp-root)

(defvar cedet-cpp-project/default-clang-flags (list))
(make-variable-buffer-local 'cedet-cpp-project/default-clang-flags)

(defclass cedet-cpp-project/project (ede-cpp-root-project)
  ((precompiled-header :initarg :precompiled-header
                       :initform nil
                       :type (or null string)
                       :documentation
                       "Precompiled header path. Is relative to the project root.")
   (extra-flags :initarg :extra-flags
                :initform nil
                :type (or null list)
                :documentation
                "Additional compiler flags.")
   )
  "ede-cpp-root-project with extra slots.")

(defmethod initialize-instance ((this cedet-cpp-project/project)
                                &rest fields)
  (call-next-method))

(defun cedet-cpp-project/dirs-parent (path)
  "Returns PATH's parent or nil."
  (let ((p (file-name-directory (directory-file-name path))))
    (if (string= p path)
        nil
      p)
    ))

(defun cedet-cpp-project/dirs-1 (path)
  "Returns the list of directories starting at PATH and going up.
PATH is used as is."
  (if path
      (cons path (cedet-cpp-project/dirs-1 (cedet-cpp-project/dirs-parent path)))
    nil))

(defun cedet-cpp-project/dirs (path)
  "Returns the list of directories starting at PATH and going up.
PATH is converted to an absolute value."
  (if path
      (cedet-cpp-project/dirs-1 (file-truename path))
    nil))

(defun cedet-cpp-project/load-project-1 (path)
  "Searches for \"project.el\" starting from PATH and going up, and loads it."
  (let ((old-dir default-directory))
    (unwind-protect
        (progn
          (setq default-directory (file-name-directory path))
          (load-file path))
      (setq default-directory old-dir))))

(defun cedet-cpp-project/load-project (&optional path)
  "Searches for project.el starting from the current directory or an arg and going up, and loads it."
  (interactive)
  (or
   (ede-current-project)
   (let ((p (locate-file "project.el" (cedet-cpp-project/dirs (or buffer-file-name path nil)))))
     (when p
       (cedet-cpp-project/load-project-1 p)
       (ede-current-project)))))

(defun cedet-cpp-project/includes-strings (project)
  (let ((includes (slot-value project :include-path))
        (root (ede-project-root-directory project)))
    (mapcar (function (lambda (x) (concat "-I" (shell-quote-argument (concat root "/" x))))) includes)))

(defun cedet-cpp-project/system-includes-strings (project)
  (let ((includes (slot-value project :system-include-path)))
    (mapcar (function (lambda (x) (concat "-I" (shell-quote-argument x)))) includes)))

(defun cedet-cpp-project/make-clang-flags (project)
  "Forms cflags for clang based on the PROJECT."
  (append
   cedet-cpp-project/default-clang-flags
   (slot-value project :extra-flags)
   (cedet-cpp-project/includes-strings project)
   (cedet-cpp-project/system-includes-strings project)))

(defun cedet-cpp-project/prefix-header (project)
  "Returns absolute precompiled hader path for the PROJECT or nil."
  (let ((path (slot-value project :precompiled-header))
        (root (ede-project-root-directory project)))
    (when path
      (concat root "/" path))))

(provide 'cedet-cpp-project)
;;; cedet-cpp-project.el ends here
