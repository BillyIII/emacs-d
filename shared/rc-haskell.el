;;; rc-haskell.el --- Haskell mode.                  -*- lexical-binding: t; -*-
;; Author: Тима <tima@tima-laptop>
;; 
;;; Code:

;; ; haskell mode
;; (add-to-list 'load-path (expand-file-name "/usr/share/emacs/site-lisp/haskell-mode"))
;; (autoload 'haskell-mode "haskell-mode" nil t)

;; (add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;; ;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;; ;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/local/ghc-mod"))
;; (require 'ghc)

(defun my/haskell-hook ()
  (rainbow-delimiters-mode)
  (highlight-parentheses-mode)
  (turn-on-haskell-indentation)
  ;; (local-set-key (kbd "C-TAB") 'ghc-complete)
  (lsp)
  )

(add-hook 'haskell-mode-hook 'my/haskell-hook)

(require 'lsp-haskell)
(setq lsp-haskell-plugin-ghcide-completions-config-auto-extend-on nil)
(setq lsp-haskell-plugin-hlint-config-flags ["--ignore=Use newtype instead of data"])
;; hls-stan-plugin ignores the config apparently
;; https://github.com/haskell/haskell-language-server/issues/3904
(setq lsp-haskell-plugin-stan-global-on nil)

(setq flycheck-hlint-ignore-rules '("Use newtype instead of data"))

(provide 'rc-haskell)
;;; rc-haskell.el ends here
