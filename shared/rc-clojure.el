;;; rc-clojure.el --- Clojure mode.                  -*- lexical-binding: t; -*-
;; Author: Тима <tima@tima-laptop>
;; 
;;; Code:

;; ;; clojure-mode
;; ;; (add-to-list 'load-path "/usr/share/emacs/site-lisp/clojure-mode")
;; ;; (require 'clojure-mode)

;; ;; (defun clojure-indent-or-expand (arg)
;; ;;   "Either indent according to mode, or expand the word preceding
;; ;; point."
;; ;;   (interactive "*P")
;; ;;   (if (and
;; ;;        (or (bobp) (= ?w (char-syntax (char-before))))
;; ;;        (or (eobp) (not (= ?w (char-syntax (char-after))))))
;; ;;       (dabbrev-expand arg)
;; ;;     (indent-according-to-mode)))

;; ;; (defun clojure-tab-fix ()
;; ;;   (local-set-key [tab] 'clojure-indent-or-expand))

;; ;; (add-hook 'emacs-lisp-mode-hook 'clojure-tab-fix)
;; ;; (add-hook 'clojure-mode-hook    'clojure-tab-fix)

(defun my/clojure-hook ()
  (rainbow-delimiters-mode)
  (highlight-parentheses-mode)
  (parscope-mode)
  )

(add-hook 'clojure-mode-hook 'my/clojure-hook)

(provide 'rc-clojure)
;;; rc-clojure.el ends here
