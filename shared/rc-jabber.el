;;; rc-jabber.el --- Jabber configuration.                   -*- lexical-binding: t; -*-

(require 'jabber)
;; (require 'jabber-carbons)

(require 'fsm)
(setq fsm-debug nil)

(setq jabber-account-list `((,(concat "BunnyInAHat@jabbers.one/" (system-name)))
                            (,(concat "BunnyInAHat@xmpp.is/" (system-name)))))
(setq jabber-default-priority 50)

(setq jabber-history-enabled t)
(setq jabber-history-muc-enabled t)

(setq jabber-roster-show-title nil)
(setq jabber-roster-line-format " %c %-25n %u %-8s  %S")
(remove-hook 'jabber-alert-presence-hooks 'jabber-presence-echo)

(setq jabber-reconnect-delay 120)
(setq jabber-auto-reconnect t)

;; (setq jabber-activity-mode t)
;; (setq jabber-alert-presence-hooks nil)

;; (setq jabber-debug-log-xml t)

;; (add-hook 'jabber-post-connect-hooks 'jabber-enable-carbons)
(add-hook 'jabber-post-connect-hooks 'jabber-autoaway-start)

(defun my/jabber-init-hook ()
  ;; Wait until the user is (hopefully) able to react to the gnupg password prompt.
  (when (selected-frame)
    (remove-hook 'after-make-frame-functions 'my/jabber-init-hook)
    (remove-hook 'after-init-hook 'my/jabber-init-hook)
    (jabber-connect-all)))

;; (add-hook 'after-make-frame-functions 'my/jabber-init-hook)
;; (add-hook 'after-init-hook 'my/jabber-init-hook)

;; jabber-read-password is bugged.
;; Required fields must be regular, non-keyword symbols.
(defun jabber-read-password (bare-jid)
  "Read Jabber password from minibuffer."
  (message "user %S host %S" (jabber-jid-username bare-jid) (jabber-jid-server bare-jid))
  (let ((found
	 (and (fboundp 'auth-source-search)
	      (nth 0 (auth-source-search
		      :user (jabber-jid-username bare-jid)
		      :host (jabber-jid-server bare-jid)
		      :port "xmpp"
		      :max 1
		      :require '(secret)))))) ;; HERE
    (if found
	(let ((secret (plist-get found :secret)))
	  (copy-sequence
	   (if (functionp secret)
	       (funcall secret)
	     secret)))
      (let ((prompt (format "Jabber password for %s: " bare-jid)))
	;; Need to copy the password, as sasl.el wants to erase it.
	(copy-sequence
	 (password-read prompt (jabber-password-key bare-jid)))))))

;; (run-with-idle-timer 1 nil 'jabber-connect-all)

(custom-set-faces
 '(jabber-title-small ((t (:inherit default :weight bold :width semi-expanded))))
 '(jabber-title-medium ((t (:inherit default :weight bold :width expanded))))
 '(jabber-title-large ((t (:inherit default :weight bold :width ultra-expanded))))
 '(jabber-roster-user-online ((t (:foreground "dark green" :weight bold))))
 '(jabber-roster-user-away ((t (:foreground "dark green"))))
 '(jabber-roster-user-xa ((t (:foreground "dark green"))))
 )

(setq jabber-presence-strings
  `(("" . ,(jabber-propertize "💡" 'face 'jabber-roster-user-online))
    ("away" . ,(jabber-propertize "💺" 'face 'jabber-roster-user-away))
    ("xa" . ,(jabber-propertize "💤" 'face 'jabber-roster-user-xa))
    ("dnd" . ,(jabber-propertize "⛔" 'face 'jabber-roster-user-dnd))
    ("chat" . ,(jabber-propertize "🗨️" 'face 'jabber-roster-user-chatty))
    ("error" . ,(jabber-propertize "⚠️" 'face 'jabber-roster-user-error))
    (nil . ,(jabber-propertize "🔌" 'face 'jabber-roster-user-offline))))

;; Default implementation has "Offline" string hardcoded.
(defun jabber-mode-line-presence-update (&rest _)
  (setq jabber-mode-line-string (if (and jabber-connections (not *jabber-disconnecting*))
				                    (cdr (assoc *jabber-current-show* jabber-presence-strings))
				                  (cdr (assoc nil jabber-presence-strings)))))
  ;; (setq jabber-mode-line-presence (if (and jabber-connections (not *jabber-disconnecting*))
  ;;   			                      (cdr (assoc *jabber-current-show* jabber-presence-strings))
  ;;   			                    (cdr (assoc nil jabber-presence-strings)))))

;; (jabber-mode-line-mode 1)

(provide 'rc-jabber)
;;; rc-jabber.el ends here
