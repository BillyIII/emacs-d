;;; yet-another-journal.el --- Yet another org-mode journal.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  BillyIII

;; Author: BillyIII <me@billyiii.me>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:


(setq yaj/dir "~/Documents/journal/")
(setq yaj/filename-format "%G.org.gpg")
(setq yaj/filename-regex "\\(?1:[[:digit:]]*\\)\\.org\\.gpg")

(setq yaj/week-heading-level 1)
(setq yaj/week-heading-format "* Week %V")
(setq yaj/week-heading-regex "\\* Week \\(?1:[[:digit:]]*\\)")

(setq yaj/day-heading-level (+ 1 yaj/week-heading-level))
(setq yaj/day-heading-format "** Day %u")
(setq yaj/day-heading-regex "\\*\\* Day \\(?1:[[:digit:]]*\\)")

(setq yaj/time-heading-format "*** %H:%M")


(defun yaj/match-number-regex (regex text)
  (string-to-number (replace-regexp-in-string regex "\\1" text)))

(defun yaj/outline-heading-text ()
  (save-excursion
    (let ((start (point)))
      (outline-end-of-heading)
      (buffer-substring-no-properties start (point)))))

(defun yaj/goto-heading (level format regex time)
  (let ((insert-at (point)))
    (outline-previous-heading)
    (let ((n (- (funcall outline-level) level)))
      (when (> n 0)
        (outline-up-heading n t)))
    (let* ((dest-heading (format-time-string format time))
           (dest-value (yaj/match-number-regex regex dest-heading))
           (run t))
      (while run
        (let ((curr-value (yaj/match-number-regex regex (yaj/outline-heading-text))))
          (if (> curr-value dest-value)
              (setq insert-at (point)
                    run (outline-get-last-sibling))
            (setq run nil)
            (when (= curr-value dest-value)
                (setq insert-at nil)))))
      (when insert-at
        (goto-char insert-at)
        (insert dest-heading "\n")
        (goto-char insert-at))
      (yaj/expand-and-advance))))

(defun yaj/goto-week-heading (time)
  (yaj/goto-heading
   yaj/week-heading-level
   yaj/week-heading-format
   yaj/week-heading-regex
   time))
    
(defun yaj/goto-day-heading (time)
  (yaj/goto-heading
   yaj/day-heading-level
   yaj/day-heading-format
   yaj/day-heading-regex
   time))

(defun yaj/goto-time-heading (time)
  (insert (format-time-string yaj/time-heading-format time) "\n")
  (yaj/expand-and-advance))

(defun yaj/expand-and-advance ()
  (outline-show-heading)
  (outline-get-next-sibling)
  (when (> (point) (line-beginning-position)) (insert "\n")))

(defun yaj/make-file-path (time)
  (expand-file-name
   (format-time-string yaj/filename-format time)
   yaj/dir))

(defun yaj/create-entry (&optional opt-time)
  (interactive)
  (let ((time (if opt-time opt-time (current-time))))
    (find-file (yaj/make-file-path time))
    
    (goto-char (point-max))
    (yaj/goto-week-heading time)
    (yaj/goto-day-heading time)
    (yaj/goto-time-heading time)
    
    (save-excursion (insert "\n"))
    (indent-for-tab-command)))


(provide 'yet-another-journal)
;;; yet-another-journal.el ends here
