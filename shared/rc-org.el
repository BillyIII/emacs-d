
(require 'org-agenda)

(setq org-hide-block-startup t)
(setq org-hide-drawer-startup t)
(setq org-startup-folded 'nofold)

(setq my/sync-org-dir (expand-file-name "~/sync/data"))

(defun my/sync-org-path (path)
  (concat my/sync-org-dir "/" path))

(setq org-enforce-todo-dependencies t)
;; (setq org-blank-before-new-entry '((heading . nil) (plain-lilst-item . nil)))
(setq org-archive-mark-done t)
(setq org-footnote-section "Footnotes")
(setq org-directory my/sync-org-dir)

(setq org-confirm-elisp-link-not-regexp
      "\\`\\((wl)\\|(elfeed)\\|(org-agenda-list)\\|(my/update-inbox)\\)\\'")

(defun my/update-inbox ()
  (interactive)
  (my/elfeed-update-with-cb
   (lambda ()
     (compile "offlineimap -q && update-all.sh"))))

(setq my/org-inbox (my/sync-org-path "inbox/inbox.org"))
(setq my/org-schedule-dir (my/sync-org-path "schedule"))
(setq my/org-schedule-regular (concat my/org-schedule-dir "/regular.org"))
(setq my/org-schedule-planned (concat my/org-schedule-dir "/planned.org"))

(setq org-agenda-files `(,my/org-schedule-dir))

(add-to-list 'org-agenda-custom-commands
       '("w" "week-span"
         ((agenda "" ))
         ((org-agenda-overriding-header "Week")
          (org-agenda-start-on-weekday nil)
          (org-agenda-span 7)
          (org-agenda-start-day "0d"))))

(defun my/open-kb-root ()
  (interactive)
  (dired (my/sync-org-path "kb")))

(setq org-capture-templates `(("t" "Todo [inbox]" entry
                               (file ,my/org-inbox)
                               "* TODO %i%?\n")
                              ("j" "Journal" entry
                               (file
                                (lambda () (my/sync-org-path
                                            (format-time-string "journal/%Y/%m.org"))))
                               "* %<%d (%A) %R>\n%i%?\n")))

;; (setq org-refile-targets '((my/gtd-projects :maxlevel . 3)
;;                            (my/gtd-someday :level . 1)
;;                            (my/gtd-tickler :maxlevel . 2)
;;                            (my/gtd-archive :level . 1)
;;                            ))

(setq org-default-notes-file my/org-inbox)

(define-key global-map "\C-cc" 'org-capture)

(require 'org-caldav)
(setq org-caldav-calendar-id "calendar")
(setq org-caldav-inbox my/org-inbox)
(setq org-caldav-files `(,my/org-schedule-regular
                         ,my/org-schedule-planned))
;; (setq org-icalendar-timezone "UTC")
(setq org-icalendar-include-todo 'all)
(setq org-caldav-sync-todo t)


(require 'org-roam)
(require 'org-roam-protocol)
(require 'org-roam-export)

(setq org-roam-directory (my/sync-org-path "roam"))

(setq org-roam-capture-templates
      `(("h" "Headline only" entry
         "* %i%? %^g\n:PROPERTIES:\n:ID: %(org-id-new)\n:END:\n"
         :target (file "%<%Y>/%<%m>.org"))
        ("t" "Headline with text" entry
         "* %? %^g\n:PROPERTIES:\n:ID: %(org-id-new)\n:END:\n%i\n"
         :target (file "%<%Y>/%<%m>.org"))))

(setq org-roam-capture-ref-templates
      `(("b" "Browser entry" entry
         "* ${title} :link:\n:PROPERTIES:\n:ID: %(org-id-new)\n:END:\n${body}\n"
         :target (file "%<%Y>/%<%m>.org"))))

(setq org-roam-node-display-template
      (concat "${title} "
              (propertize "${tags}" 'face 'org-tag)))

(org-roam-db-autosync-mode)


(provide 'rc-org)
