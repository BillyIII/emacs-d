;; -*- lexical-binding: t -*-

(setq my/gui-symbols (list 'set-fontset-font 'tool-bar-mode 'set-scroll-bar-mode))

(defun my/nop (&optional a b c d e f g h i j k)
  "my/nop"
  (message "nop"))

(dolist (s my/gui-symbols)
  (when (not (fboundp s))
    (fset s 'my/nop))
  (when (not (boundp s))
    (set s nil)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.)
)
 ;; '(parscope-overlay-face ((t (:background "magenta4"))))
 ;; '(powerline-active1 ((t (:inherit mode-line :background "cyan"))))
 ;; '(powerline-active2 ((t (:inherit mode-line :background "darkcyan"))))
 ;; '(powerline-inactive1 ((t (:inherit mode-line :background "torquoise"))))
 ;; '(powerline-inactive2 ((t (:inherit mode-line :background "darktorquoise")))))

 ;; '(ansi-color-names-vector
 ;;   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 ;; '(ansi-term-color-vector
 ;;   [unspecified "#081724" "#ff694d" "#68f6cb" "#fffe4e" "#bad6e2" "#afc0fd" "#d2f1ff" "#d3f9ee"] t)
 ;; '(vc-annotate-background "#2b2b2b")
 ;; '(vc-annotate-color-map
 ;;   '((20 . "#bc8383")
 ;;     (40 . "#cc9393")
 ;;     (60 . "#dfaf8f")
 ;;     (80 . "#d0bf8f")
 ;;     (100 . "#e0cf9f")
 ;;     (120 . "#f0dfaf")
 ;;     (140 . "#5f7f5f")
 ;;     (160 . "#7f9f7f")
 ;;     (180 . "#8fb28f")
 ;;     (200 . "#9fc59f")
 ;;     (220 . "#afd8af")
 ;;     (240 . "#bfebbf")
 ;;     (260 . "#93e0e3")
 ;;     (280 . "#6ca0a3")
 ;;     (300 . "#7cb8bb")
 ;;     (320 . "#8cd0d3")
 ;;     (340 . "#94bff3")
 ;;     (360 . "#dc8cc3")))
 ;; '(vc-annotate-very-old-color "#dc8cc3")
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-default nil)
 '(backup-inhibited t t)
 '(column-number-mode t)
 '(custom-safe-themes
   '("68b3feafedf988b4c049f2fd985dd6f6b21d0971706bd581a74f000bd1c5877c" "2b80b8864ae9fa004bf55bb6d30bb948621f85eb23ee80a1d7bf071d0e4b51e1" "68769179097d800e415631967544f8b2001dae07972939446e21438b1010748c" "3ff1b0807cfd7576985073cb9ef977bc10b0b5375a87a85d0faa7b9865b54a5d" "246a51f19b632c27d7071877ea99805d4f8131b0ff7acb8a607d4fd1c101e163" "4c9ba94db23a0a3dea88ee80f41d9478c151b07cb6640b33bfc38be7c2415cc4" "5e1d1564b6a2435a2054aa345e81c89539a72c4cad8536cfe02583e0b7d5e2fa" default))
 '(echo-keystrokes 0.01)
 '(fci-rule-color "#383838")
 '(fill-column 78)
 '(frame-title-format '("%f - " user-real-login-name "@" system-name) t)
 '(grep-highlight-matches nil)
 '(ido-auto-merge-work-directories-length nil)
 '(ido-create-new-buffer 'always)
 '(ido-enable-flex-matching t)
 '(ido-enable-prefix nil)
 '(ido-everywhere t)
 '(ido-ignore-extensions t)
 '(ido-max-prospects 8)
 '(ido-use-filename-at-point 'guess)
 '(indent-tabs-mode nil)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(linum-format "  %d  ")
 '(muse-project-alist nil)
 '(puppet-indent-level tab-width)
 '(recentf-max-saved-items 75)
 '(require-final-newline t)
 '(ruby-indent-level tab-width)
 '(safe-local-variable-values
   '((package-lint-main-file . "mastodon.el")
     (haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)))
 '(save-place t)
 '(send-mail-function 'sendmail-send-it)
 '(show-paren-delay 0)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(w3m-use-cookies t)
 '(w3m-use-favicon nil))

;; (load-file "/home/tima/src/emacspeak/lisp/emacspeak-setup.el")

;;
;; Utilities for host-specific values.
;;

(setq my/system-name-aliases '())
;; ("hostname" . symbol)

(defun my/system-name ()
  (interactive)
  (let* ((str (system-name))
         (alias (cdr (assoc-string str my/system-name-aliases t))))
    (if (not (eq alias nil))
        alias
        (intern str))))

(setq my/system-name (my/system-name))

(defun my/alist-get-system (alist)
  (interactive)
  (alist-get (my/system-name) alist (alist-get t alist)))


;; disable menu- and tool-bar
(tool-bar-mode 0)
(set-scroll-bar-mode 'left)
(menu-bar-mode 0)

(require 'server)
(unless (server-running-p)
 (server-start))

;; (setq my/emacspeak-el (expand-file-name "~/src/emacspeak/lisp/emacspeak-setup.el"))

;; (when (file-exists-p my/emacspeak-el)
;;  (load-file my/emacspeak-el))

;;
;; Packages.
;;

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq my/straight-packages '(auto-complete bbcode-mode blimp browse-kill-ring chee clojure-mode cm-mode cmake-mode coffee-mode color-theme column-enforce-mode company-irony company-qml consult csharp-mode dante dash delight dropbox eat ef-themes eimp electric-case elein elfeed elfeed-goodies elpher elvish-mode emacs-everywhere embark embark-consult enh-ruby-mode eshell-autojump eshell-bookmark eww-lnum find-file-in-project flex-autopair flycheck-irony fuzzy gemini-mode gnugo gnuplot gnuplot-mode go graphviz-dot-mode haskell-mode hide-lines highlight-parentheses highlight-symbol htmlize image+ image-dired+ irony irony-eldoc iscroll jabber json-mode less-css-mode load-theme-buffer-local look-mode lsp-haskell lsp-mode lua-mode magit marginalia markdown-mode mastodon mingus mixed-pitch monokai-theme mpdel mpv muse nameless names nov orderless org-board org-caldav org-chef org-contacts org-journal org-ml org-present org-present-remote org-ql ox-bb ox-gemini paredit password-store persp-mode pos-tip powerline projectile prop-menu qml-mode qt-pro-mode racket-mode rainbow-delimiters ranger rpm-spec-mode rust-mode rustic s sass-mode selectric-mode shimbun smart-forward smex tabula-rasa telega telepathy todotxt transducers vertico vulpea w3m wanderlust which-key writeroom-mode yaml-mode yasnippet yasnippet-snippets zeal-at-point zen-and-art-theme jinx ement denote consult-notes dart-mode))

;; unicode-fonts
;; eplot
;; org-roam org-roam-timestamps org-roam-ui 

;; (straight-pull-recipe-repositories)

;; Don't update org-mode automatically.
(straight-use-package '(org :type built-in))

(dolist (package my/straight-packages)
  (straight-use-package package))

(straight-use-package '(flix-mode
                        :type git
                        :host github
                        :repo "jhckragh/flix-mode"))

(when (featurep 'native-compile)
  (require 'comp)
  (native-compile-prune-cache))

;;
;; Fonts
;;

(set-fontset-font t 'cyrillic "ADYS")

;; Проверка 🖖 связи
;; (set-fontset-font t 'unicode (font-spec :family "DejaVu Sans Mono"))
;; (set-fontset-font t 'emoji (font-spec :family "JoyPixels"))

;; (setq unicode-fonts-skip-font-groups nil)

;; (require 'unicode-fonts)
;; (unicode-fonts-setup)

(require 'mixed-pitch)
(setq mixed-pitch-variable-pitch-cursor nil)
(add-hook 'org-mode-hook 'mixed-pitch-mode)

(setq frame-resize-pixelwise t)

;; Because interrupting execution with an unskippable modal UI is such a great idea.
;; Especially when there is no UI and the code in question is some background task.
;; Obnoxious asshat should switch to Apple (c)(r)(tm) ShitWare (c)(r)(tm).
(setq tags-add-tables nil)
;; Ditto.
(setq persp-kill-foreign-buffer-behaviour nil)
(setq persp-auto-save-persps-to-their-file t)
(setq persp-auto-save-persps-to-their-file-before-kill t)

(add-to-list 'load-path (expand-file-name "~/.emacs.d/shared"))

(require 'rc-input)

(setq mode-require-final-newline 'ask)


;; http://xahlee.info/emacs/emacs/emacs_init_whitespace_mode.html
;; char	codepoint (decimal)	name
;; ·	183	MIDDLE DOT
;; ¶	182	PILCROW SIGN
;; ↵	8629	DOWNWARDS ARROW WITH CORNER LEFTWARDS
;; ↩	8617	LEFTWARDS ARROW WITH HOOK
;; ⏎	9166	RETURN SYMBOL
;; ▷	9655	WHITE RIGHT POINTING TRIANGLE
;; ▶	9654	BLACK RIGHT-POINTING TRIANGLE
;; →	8594	RIGHTWARDS ARROW
;; ↦	8614	RIGHTWARDS ARROW FROM BAR
;; ⇥	8677	RIGHTWARDS ARROW TO BAR
;; ⇨	8680	RIGHTWARDS WHITE ARROW
(setq whitespace-display-mappings
      '(
        (space-mark 32 [183])
        (newline-mark 10 [182 10])
        (tab-mark 9 [9655 9])
        ))

(setq whitespace-style '(face missing-newline-at-eof indentation::space))

(defun my/prog-mode-hook ()
	  ;; (set (make-local-variable 'whitespace-style)
  ;;      '(face missing-newline-at-eof tab-mark))
  (whitespace-mode 1)
  (setq show-trailing-whitespace t))

(add-hook 'prog-mode-hook 'my/prog-mode-hook)


;; TODO: embark config

;;
;; Completion
;;

(require 'consult)
(require 'vertico)
(require 'marginalia)
(require 'orderless)
(require 'embark)
(require 'embark-consult)

(vertico-mode t)
(marginalia-mode t)

(setq enable-recursive-minibuffers t)

(setq completion-styles '(orderless basic))
;; (setq completion-category-defaults nil)
(setq completion-category-overrides '((file (styles partial-completion))))
(setq consult-narrow-key "<")
(setq consult-preview-key ">")

(define-key vertico-map [M-c] #'vertico-previous)
(define-key vertico-map [M-t] #'vertico-next)

(xah-fly--define-keys
 xah-fly-leader-key-map
 '(("u" . consult-buffer)
   ;; ("c ." . consult-find)
   ;; ("c ." . find-file)
   ))

;; ;; elim
;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/shared/elim"))
;; (setq garak-icon-directory (expand-file-name "~/.emacs.d/shared/elim/icons/"))
;; (setq elim-executable (expand-file-name "~/.emacs.d/shared/elim/elim-client"))
;; (autoload 'garak "garak" nil t)

;; ; g-client
;; ;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/shared/g-client"))
;; ;; (load "gnotebook")

;; slime
;;(load "rc-slime.el")

;;(require 'rc-wl)
(require 'rc-www)
;; (require 'darkroom)
;; (require 'writeroom)

;; (require 'color-theme)

;; (defun fixed-face-spec-set (face spec frame)
;;   ;; (put face 'face-override-spec spec)
;;   ;; (make-empty-face face)
;;   (face-spec-recalc face frame)
;;   ;; (put face 'face-override-spec nil)
;;   ;; (face-spec-set face spec frame)
;;   (while (get face 'face-alias)
;;     (setq face (get face 'face-alias)))
;;   (setq spec (face-spec-choose spec frame))
;;   ;; (message "face: %s, spec: %s" face spec)
;;   (face-spec-set-2 face frame spec)
;;   )

;; ;; Redefines the color-theme function.
;; ;; Calls fixed-face-spec-set instead of (now incompatible) face-spec-set.
;; (defun color-theme-install-faces (faces)
;;   ;; clear all previous faces
;;   (when (not color-theme-is-cumulative)
;;     (color-theme-reset-faces))
;;   ;; install new faces
;;   (let ((faces (color-theme-filter faces color-theme-illegal-faces t))
;;         (frame (if color-theme-is-global nil (selected-frame))))
;;     (dolist (entry faces)
;;       (let ((face (nth 0 entry))
;;             (spec (nth 1 entry)))
;;         (or (facep face)
;;             (make-empty-face face))
;;         ;; remove weird properties from the default face only
;;         (when (eq face 'default)
;;           (setq spec (color-theme-spec-filter spec)))
;;         ;; Emacs/XEmacs customization issues: filter out :bold when
;;         ;; the spec contains :weight, etc, such that the spec remains
;;         ;; "valid" for custom.
;;         (setq spec (color-theme-spec-compat spec))
;;         ;; using a spec of ((t (nil))) to reset a face doesn't work
;;         ;; in Emacs 21, we use the new function face-spec-reset-face
;;         ;; instead
;;         (if (and (functionp 'face-spec-reset-face)
;;                  (equal spec '((t (nil)))))
;;             (face-spec-reset-face face frame)
;;           (condition-case var
;;               (progn
;;                 (fixed-face-spec-set face spec frame)
;;                 (if color-theme-is-global
;;                     (put face 'face-defface-spec spec)))
;;             (error (message "Error using spec %S: %S" spec var))))))))

;; ;; (load-theme 'tronesque)
;; (require 'color-theme-tronesque)

(setq my/font-sizes '((pcarch . 211)
                      (pcvoid . 211)
                      (tima-laptop . 203)
                      (netbook.home . 160)
                      (t . 203)))

(setq my/graphical-themes '((t . color-theme-tronesque)))

(setq my/terminal-themes '((t . nil)))

(defun my/disable-all-themes ()
  (interactive)
  (dolist (t custom-enabled-themes) (disable-theme t)))

(set-face-attribute 'default nil :height (my/alist-get-system my/font-sizes))

(require 'ef-themes)
(setq ef-themes-mixed-fonts t)
(setq ef-themes-variable-pitch-ui nil)
(custom-set-faces
 '(ef-themes-ui-variable-pitch ((t (:inherit fixed-pitch :height 0.7)))))
(ef-themes-select 'ef-elea-light)

(defun my/set-theme ()
  (interactive)
  ;; (my/disable-all-themes)
  (let* ((themes-list (if (window-system (selected-frame))
                          my/graphical-themes
                        my/terminal-themes))
         (theme (my/alist-get-system themes-list)))
    (message "Setting theme %s for frame %s" theme (selected-frame))
    (when theme
      (let ((color-theme-is-global nil)
            (color-theme-is-cumulative t))
        (funcall theme))))
  (set-face-attribute 'default nil :height (my/alist-get-system my/font-sizes)))
    ;; (let ((theme (my/alist-get-system my/themes)))
    ;;          (when theme (load-theme 'tronesque t))))
  ;; (set-face-attribute 'default nil :height (my/alist-get-system my/font-sizes)))

;;(add-hook 'after-make-frame-functions
;;          (lambda (frame)
;;            (with-selected-frame frame
;;              (my/set-theme))))
;;(if (not (daemonp))
;;    (my/set-theme))

(put 'buffer-face-mode-face 'safe-local-variable 'plistp)
(put 'line-spacing 'safe-local-variable 'natnump)

;; (require 'powerline)
;; (setq powerline-default-separator 'slant)
;; (powerline-default-theme)

(require 'rainbow-delimiters)
(require 'highlight-parentheses)
;; (require 'highline)
(require 'electric-case)
;;(require 'parscope)

(require 'delight)
(delight '((which-key-mode nil which-key)
           (xah-fly-keys nil xah-fly-keys)))

(require 'yasnippet)
(yas-global-mode 1)

;; ;; transparent encryption
(require 'epa-file)
;; (require 'muse-mode)
(epa-file-enable)
(setq epa-file-cache-passphrase-for-symmetric-encryption t)

(require 'persp-mode)
;; (require 'projectile)
;; (require 'ido)

(define-key persp-key-map (kbd "N") 'persp-add-new)
;; This doesn't seem to be hooked into ido in any way atm.
(setq persp-toggle-read-buffer-filter-keys (kbd "M-u"))
;; Add it manually.
;; (define-key ido-common-completion-map (kbd "M-u") 'ido-toggle-persp-filter)

(persp-mode 1)
(persp-set-read-buffer-function t)
(persp-set-ido-hooks t)

(require 'auth-source-pass)
(auth-source-pass-enable)

(setq password-cache t)
(setq password-cache-expiry nil)

;; (defun add-note ()
;;   "Create a new note as ~/.notes/year/month/day/HH_MM_SS.org.gpg"
;;   (interactive)
;;   (let* ((dir (expand-file-name (format-time-string "~/.notes/%Y/%m/%d" (current-time))))
;;          (file (concat dir (format-time-string "/%H_%M_%S.org.gpg" (current-time))))
;;          )
;;     (make-directory dir t)
;;     (find-file file)
;;     (org-mode)
;;     (insert "#title " (format-time-string "%A, %B %e, %Y") "\n\n")
;;     )
;;   )

(defun insert-time ()
  "Insert a time string."
  (interactive)
  (insert (format-time-string "%H:%M:%S") " "))

(require 'nameless)
(add-hook 'emacs-lisp-mode-hook #'nameless-mode)
(setq nameless-private-prefix t)

(require 'rc-c-common)

(setq gdb-many-windows t)
(put 'narrow-to-region 'disabled nil)

(require 'rc-clojure)

(require 'rc-haskell)

(require 'rc-rust)

(setq undo-limit 200000)
(setq undo-strong-limit 360000)

;; Disable the GUI prompts.
(setq use-dialog-box nil)

(savehist-mode 1)
;; (desktop-save-mode 1)
;; (ido-mode 1)
(column-number-mode 1)

(auto-insert-mode)
(add-hook 'find-file-hook 'auto-insert)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; (require 'cursor-chg)  ; Load the library
;; (toggle-cursor-type-when-idle 1) ; Turn on cursor change when Emacs is idle
;; (change-cursor-mode 1) ; Turn on change for overwrite, read-only, and input mode

(require 'rc-zone)

(setq flyspell-use-meta-tab nil)

(setq set-mark-command-repeat-pop t)

(require 'which-key)
(which-key-mode)

;; (require 'org-caldav)
;; (require 'rc-wl)

;; (substitute-key-definition 'ido-prev-match ido-common-completion-map)
;; (substitute-key-definition 'ido-next-match ido-common-completion-map)

(defun my/paste-primary-selection ()
  (interactive)
  (insert
   (x-get-selection 'PRIMARY)))

(setq kill-ring-max 600)

(setq mail-host-address "gmail.com")
(setq user-full-name "BillyIII")
(setq user-mail-address "zato.two@gmail.com")

(setq unison-args '("batch"))

(setq mm-decrypt-option 'always)
(setq mm-verify-option 'always)
(setq mm-sign-option 'guided)
(setq gnus-buttonized-mime-types '("multipart/encrypted" "multipart/signed"))
(setq mml-smime-use 'epg)
(setq epg-debug t)

(require 'rc-org)

(require 'rc-files)

(require 'elfeed)
;; (require 'elfeed-goodies)
;; (elfeed-goodies/setup)
;; (setq elfeed-goodies/switch-to-entry nil)
;; (setq elfeed-goodies/entry-pane-position 'bottom)

(setq my/oneshot-elfeed-update-cb nil)

(defun my/oneshot-elfeed-update-hook (url)
  (if-let ((cb my/oneshot-elfeed-update-cb))
      (if (> (length elfeed-curl-queue) 0)
          (message "Elfeed is updating...")
        (message "Elfeed update is done.")
        (setq my/oneshot-elfeed-update-cb nil)
        (funcall cb))))

(add-hook 'elfeed-update-hooks 'my/oneshot-elfeed-update-hook)

(defun my/elfeed-update-with-cb (cb)
  (if my/oneshot-elfeed-update-cb
      (error "Another elfeed update is already running.")
    (setq my/oneshot-elfeed-update-cb cb)
    (elfeed-update)))

; (load "~/.emacs.d/shared/proofgeneral/generic/proof-site")

(setq company-idle-delay nil)
(setq company-tooltip-idle-delay nil)
;; Documentation says that zero disables the timer, but it makes it fire immediately instead.
;; (setq eldoc-idle-delay 0)
(setq eldoc-idle-delay 100000000)

(require 'tramp)
(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)

(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-remove-inline-images)
                 (org-present-show-cursor)
                 (org-present-read-write)))))

;; (load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")
(defun my/nyxt-shell () (interactive) (slime "nyxt-shell.sh"))

;; (add-to-list 'auto-mode-alist '("\\.rb\\'" . enh-ruby-mode))
(add-to-list 'auto-mode-alist
             '("\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'" . enh-ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))

;; Extra selected packages: prop-menu.
(add-to-list 'load-path "~/.emacs.d/shared/idris2-mode")
(require 'idris2-mode)

(require 'lsp-mode)
(defun my/idris2-mode-hook ()
  ;; (lsp-mode)
  )

(add-hook 'idris2-mode-hook 'my/idris2-mode-hook)

(require 'rc-jabber)
(require 'rc-telega)

(require 'rc-term)


(require 'jinx)

(setq jinx-languages "en_US ru_RU")

(add-hook 'org-mode-hook 'jinx-mode)
(add-hook 'text-mode-hook 'jinx-mode)
(add-hook 'markdown-mode-hook 'jinx-mode)


(add-to-list 'load-path (expand-file-name "~/.emacs.d/local"))
(require 'local nil t)

;; (require 'server)
;; ;; (setq server-use-tcp t)
;; (unless (server-running-p)
;;  (server-start))
